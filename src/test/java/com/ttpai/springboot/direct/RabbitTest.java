package com.ttpai.springboot.direct;

import com.ttpai.springboot.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class RabbitTest {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Test
    public void sendDirectTest() throws InterruptedException {

        String context = "此消息在，direct的交换机模式队列下，有 DirectReceiver 可以收到";

        String routeKey = "direct";

        String exchange = "boot_direct_exchange";

        context = "context:" + exchange + ",routeKey:" + routeKey + ",context:" + context;

        this.rabbitTemplate.convertAndSend(exchange,routeKey,context);

        System.out.println("==========sendDirectTest : " + context);


    }

    @Test
    public void sendDirectTest2(){

        String context = "此消息在，direct的交换机模式队列下，有 DirectReceiver 可以收到";

        String routeKey = "direct";

        String exchange = "boot_direct_exchange";

        context = "context:" + exchange + ",routeKey:" + routeKey + ",context:" + context;

        this.rabbitTemplate.convertAndSend(exchange,routeKey,context);

        System.out.println("==========sendDirectTest : " + context);


    }


    @Test
    public void sendTopicTest1(){

        String context = "topic模式";

        String routeKey = "topic.message";

        String exchange = "boot_topic_exchange";

        context = "context:" + exchange + ",routeKey:" + routeKey + ",context:" + context;

        for(int i=0; i<50; i++){

            this.rabbitTemplate.convertAndSend(exchange,routeKey,context + i);
        }


        System.out.println("==========sendTopictTest1 : " + context);

    }


    @Test
    public void sendTopicTest2(){

        String context = "topic模式";

        String routeKey = "topic.message.gsb.test";

        String exchange = "boot_topic_exchange";

        context = "context:" + exchange + ",routeKey:" + routeKey + ",context:" + context;

        this.rabbitTemplate.convertAndSend(exchange,routeKey,context);

        System.out.println("==========sendTopictTest2 : " + context);

    }


    @Test
    public void sendTopicTest3(){

        String context = "topic模式";

        String routeKey = "topic.sm.gsb.test";

        String exchange = "boot_topic_exchange";

        context = "context:" + exchange + ",routeKey:" + routeKey + ",context:" + context;

        this.rabbitTemplate.convertAndSend(exchange,routeKey,context);

        System.out.println("==========sendTopictTest3 : " + context);

    }



    @Test
    public void sendMessageTest() {

        String context = "此消息在，配置转发消息模式队列下， 有 TopicReceiver1 TopicReceiver2 TopicReceiver3 可以收到";

        String routeKey = "topic.message";

        String exchange = "boot_topic_exchange";

        context = "context:" + exchange + ",routeKey:" + routeKey + ",context:" + context;

        System.out.println("sendMessageTest : " + context);

        this.rabbitTemplate.convertAndSend(exchange, routeKey, context);
    }
}
