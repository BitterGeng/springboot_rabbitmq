package com.ttpai.springboot.service;

import com.ttpai.springboot.bean.Person;
import com.ttpai.springboot.confirrm.RabbitConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author songbo.geng
 * @date 2018/9/7
 */

@Service
public class HelloSender  implements RabbitTemplate.ReturnCallback, RabbitTemplate.ConfirmCallback,InitializingBean {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private AtomicInteger atomicInteger = new AtomicInteger(0);


    public void send(String sendMsg) throws IOException {

        for(int i=0;i<5;i++){
            Person p = new Person(i);
            byte[] bytesFromObject = getBytesFromObject(p);
            System.out.println(atomicInteger.getAndIncrement()+"发送的内容如下:" + sendMsg+i);
            rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_NAME,"confirm.topic.demo",bytesFromObject);
        }
    }


    /**
     * 将对象进行序列化
     * @param obj
     * @return
     * @throws IOException
     */
    private byte[] getBytesFromObject(Serializable obj) throws IOException {
        if (obj == null) {
            return null;
        }
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        ObjectOutputStream oo = new ObjectOutputStream(bo);
        oo.writeObject(obj);
        return bo.toByteArray();
    }



    // 实现ReturnCallback
    // 当消息发送出去找不到对应路由队列时，将会把消息退回
    // 如果有任何一个路由队列接收投递消息成功，则不会退回消息

    @Override
    public void returnedMessage(Message message, int i, String s, String s1, String s2) {
        System.out.println("消息发送失败: " + Arrays.toString(message.getBody()));

    }


    // 实现ConfirmCallback
    // ACK=true仅仅标示消息已被Broker接收到，并不表示已成功投放至消息队列中
    // ACK=false标示消息由于Broker处理错误，消息并未处理成功

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if(null != correlationData){
            System.out.println("消息id:" + correlationData.getId());
        }
        if (ack) {
//            System.out.println("消息发送确认成功");
        } else {
            System.out.println("消息发送确认失败:" + cause);
        }

    }


    @Override
    public void afterPropertiesSet() throws Exception {
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnCallback(this);

    }
}
