package com.ttpai.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */

@SpringBootApplication
@ComponentScan("com.ttpai.springboot")
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class,args);
        System.out.println("11111111");
        System.out.println("启动了....");
    }
}
