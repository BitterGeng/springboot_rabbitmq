package com.ttpai.springboot.direct;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */
@Configuration
public class ReveiveConf {

    private static final String QUEUE_NAME = "boot_direct";

    @Bean
    public Queue directQueue(){
        return new Queue(QUEUE_NAME);
    }


    @Bean
    DirectExchange directExchange(){
        return new DirectExchange("boot_direct_exchange");
    }


    @Bean
    Binding bindingExchangeDirectQueue(Queue directQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(directQueue).to(directExchange).with("direct");
    }

}
