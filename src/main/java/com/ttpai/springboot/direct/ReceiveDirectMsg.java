package com.ttpai.springboot.direct;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */
@Component
@RabbitListener(queues = "boot_direct")
public class ReceiveDirectMsg {


    @RabbitHandler
    public void DirectReceiver(String msg){

        System.out.println("收到的信息是:  "+msg);


    }
}
