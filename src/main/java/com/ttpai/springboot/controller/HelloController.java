package com.ttpai.springboot.controller;

import com.ttpai.springboot.service.HelloSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */
@RestController
public class HelloController {


    @Autowired
    HelloSender helloSender;

    @GetMapping("/boot/persons")
    public String getAll(String msg) {

        try {
            helloSender.send(msg);
            return "ok";
        } catch (Exception e) {
            e.printStackTrace();
            return "fail";
        }
    }
}
