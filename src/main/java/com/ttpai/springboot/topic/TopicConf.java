package com.ttpai.springboot.topic;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */
@Configuration
public class TopicConf {

     static final String QUEUE_NAME1 = "boot_topic";

    private static final String QUEUE_NAME2 = "boot_topic_s";

    private static final String QUEUE_NAME3 = "boot_topic_ss";


    @Bean
    public Queue topicQueue(){
        return new Queue(TopicConf.QUEUE_NAME1,false,false,true);
    }

    @Bean
    public Queue topicQueue1(){
        return new Queue(TopicConf.QUEUE_NAME2,false,false,true);
    }


    @Bean
    public Queue topicQueue2(){
        return new Queue(TopicConf.QUEUE_NAME3,false,false,true);
    }


    /**
     * 交换机(Exchange) 描述：接收消息并且转发到绑定的队列，交换机不存储消息
     */
    @Bean
    TopicExchange topicExchange(){
        return new TopicExchange("boot_topic_exchange");
    }


    @Bean
    Binding bindingExchangeMessage(Queue topicQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(topicQueue).to(topicExchange).with("topic.message");
    }

    @Bean
    Binding bindingExchangeMessage2(Queue topicQueue1, TopicExchange topicExchange) {
        return BindingBuilder.bind(topicQueue1).to(topicExchange).with("topic.gsb.#");
    }

    @Bean
    Binding bindingExchangeMessage3(Queue topicQueue2, TopicExchange topicExchange) {
        return BindingBuilder.bind(topicQueue2).to(topicExchange).with("topic.sm.#");
    }
}
