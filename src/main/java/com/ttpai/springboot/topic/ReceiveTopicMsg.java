package com.ttpai.springboot.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */

@Component
@RabbitListener(queues = "boot_topic")
public class ReceiveTopicMsg {

    @RabbitHandler
    public void getMsg(String message){

        System.out.println("ReceiveTopicMsg收到的信息是:"+message+"==========");

    }

}
