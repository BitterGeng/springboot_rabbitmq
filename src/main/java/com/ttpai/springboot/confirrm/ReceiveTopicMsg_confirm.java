package com.ttpai.springboot.confirrm;


import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */

@Component

public class ReceiveTopicMsg_confirm {
    @RabbitListener(queues = "confirm.topic_queue",containerFactory = "jsaFactory")
    @RabbitHandler
    public void getMsg(String msg, Channel channel, Message message) throws IOException {
        System.out.println("ReceiveTopicMsg_confirm 收到的信息是:"+msg+"==========");
//        channel.basicQos(1);  //设置当前消费者同时只能拿到一个消息
        try {
//            //告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
            Thread.sleep(1000);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
//            System.out.println("receiver success");
        } catch (IOException e) {

            e.printStackTrace();
            //丢弃这条消息
            //channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
            System.out.println("receiver fail");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
