package com.ttpai.springboot.confirrm;


import com.rabbitmq.client.Channel;
import com.ttpai.springboot.bean.Person;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * @author songbo.geng
 * @date 2018/9/6
 */

@Component
@RabbitListener(queues = "confirm.topic_queue",containerFactory = "jsaFactory")
public class ReceiveTopicMsg_confirm1 {

    @RabbitHandler
    public void getMsg(byte[] msg, Channel channel, Message message) throws Exception {

        Person objectFromBytes = (Person)getObjectFromBytes(msg);

        System.out.println("ReceiveTopicMsg_confirm1 收到的信息是:"+objectFromBytes.getAge());
        try {
//            告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
            channel.basicQos(1);
            Thread.sleep(500);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e) {
            e.printStackTrace();
            //重回队列
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            System.out.println("receiver fail");
        }
    }


    //字节码转化为对象
    public  Object getObjectFromBytes(byte[] objBytes) throws Exception {
        if (objBytes == null || objBytes.length == 0) {
            return null;
        }
        ByteArrayInputStream bi = new ByteArrayInputStream(objBytes);
        ObjectInputStream oi = new ObjectInputStream(bi);
        return oi.readObject();
    }

}
