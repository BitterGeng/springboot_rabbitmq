package com.ttpai.springboot.confirrm;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author songbo.geng
 * @date 2018/9/7
 */

@Configuration
public class RabbitConfig {


    @Value("${spring.rabbitmq.host}")
    private String addresses;

    @Value("${spring.rabbitmq.port}")
    private String port;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.virtual-host}")
    private String virtualHost;

    @Value("${spring.rabbitmq.publisher-confirms}")
    private boolean publisherConfirms;

    public final static String EXCHANGE_NAME = "confirm.topic";
    public final static String QUEUE_NAME = "confirm.topic_queue";
    public final static String QUEUE_NAME_1 = "confirm.topic1_queue";
    public final static String ROUTING_KEY = "confirm.topic.#";
    public final static String ROUTING_KEY_1 = "confirm.topic.*";


    @Bean
    public ConnectionFactory connectionFactory() {

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setAddresses(addresses + ":" + port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);
        /** 如果要进行消息回调，则这里必须要设置为true */
        connectionFactory.setPublisherConfirms(publisherConfirms);
        return connectionFactory;

    }


    @Bean
    /** 因为要设置回调类，所以应是prototype类型，如果是singleton类型，则回调类为最后一次设置 */
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        return template;
    }


    /**
     * 定义交换机
     * @return
     */
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGE_NAME);
    }

    /**
     * 定义队列
     * @return
     */
    @Bean
    public Queue queue() {
        return new Queue(QUEUE_NAME, false);
    }


    /**
     * 定义队列
     * @return
     */
    @Bean
    public Queue queue1() {
        return new Queue(QUEUE_NAME_1, false);
    }

    /**
     * 定义队列绑定交换机的routingkey
     * @return
     */
    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange()).with(ROUTING_KEY);
    }

    @Bean
    public Binding binding1() {
        return BindingBuilder.bind(queue1()).to(exchange()).with(ROUTING_KEY_1);
    }



    @Bean
    public RabbitListenerContainerFactory<SimpleMessageListenerContainer> jsaFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        //prefetchCount的值设为1 这告诉RabbitMQ不要同时将多个消息分派给一个工作者
        //换句话说，在某个工作者处理完一条消息并确认它之前，RabbitMQ不会给该工作者分派新的消息
        //而是将新的消息分派给下一个不是很繁忙的工作者 这就是fair dispatch

        //如果不设置，默认是round-robin
        factory.setPrefetchCount(1);
        System.out.println("+++++++++++++++++++++++++++=");

        //AcknowledgeMode.AUTO 队列把消息发送给消费者之后就从队列中删除消息
        //AcknowledgeMode.MANUAL 关闭自动应答，说明队列把消息给消费者之后要等待消费者的确认处理完成消息。
        //如果未收到消费者的确认处理完成消息，就会分发给其他消费者，保证消息不丢失。
        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        return factory;
    }


//    @Bean
//    public MessageConverter jsonMessageConverter(){
//        return new Jackson2JsonMessageConverter();
//    }


    /**
     * 消费队列监听
     * @return
     */
//    @Bean
//    public SimpleMessageListenerContainer messageContainer() {
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory());
//        container.setQueues(queue());
//        container.setExposeListenerChannel(true);
//        container.setMaxConcurrentConsumers(5);
//        container.setConcurrentConsumers(5);//设置并发消费者数
//        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);//设置手动发送消费结果通知
//        container.setMessageListener(new ChannelAwareMessageListener() {
//
//            public void onMessage(Message message, com.rabbitmq.client.Channel channel) throws Exception {
//                try {
////                    System.out.println(
////                            "消费端接收到消息:" + message.getMessageProperties() + ":" + new String(message.getBody()));
////                    System.out.println("topic:"+message.getMessageProperties().getReceivedRoutingKey());
//                    // deliveryTag是消息传送的次数，我这里是为了让消息队列的第一个消息到达的时候抛出异常，处理异常让消息重新回到队列，然后再次抛出异常，处理异常拒绝让消息重回队列
//					/*if (message.getMessageProperties().getDeliveryTag() == 1
//							|| message.getMessageProperties().getDeliveryTag() == 2) {
//						throw new Exception();
//					}*/
//                    System.out.println("消费端接收到消息:"+new String(message.getBody()));
//                    channel.basicAck(message.getMessageProperties().getDeliveryTag(), false); // false只确认当前一个消息收到，true确认所有consumer获得的消息
//                } catch (Exception e) {
//                    e.printStackTrace();
//
//                    if (message.getMessageProperties().getRedelivered()) {
//                        System.out.println("消息已重复处理失败,拒绝再次接收...");
//                        channel.basicReject(message.getMessageProperties().getDeliveryTag(), true); // 拒绝消息
//                    } else {
//                        System.out.println("消息即将再次返回队列处理...");
//                        channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true); // requeue为是否重新回到队列
//                    }
//                }
//            }
//        });
//        return container;
//    }


}
